**The Snakemake wrapper repository has been migrated to Github: https://github.com/snakemake/snakemake-wrappers!**
Please file any issues and pull requests over there.
This repository will stay for archiving purposes.